
/// \file exampleB2a.cc
/// \brief Main program of the B2a example

#include "ActionInitialization.hh"
#include "Geometry.hh"

#include "FTFP_BERT.hh"
#include "G4OpticalPhysics.hh"
#include "G4RunManagerFactory.hh"
#include "G4StepLimiterPhysics.hh"
#include "G4SteppingVerbose.hh"
#include "G4UImanager.hh"

#include "Randomize.hh"

#include "G4ScoringManager.hh"
#include "G4UIExecutive.hh"
#include "G4VisExecutive.hh"

#include <loguru.hpp>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

int main(int argc, char **argv)
{
  // Setup logger
  // Geant4のデフォルト出力でいろいろ埋もれてしまうため、
  // 確認したい内容はファイルにログを残すことにした。
  // サイズなどによるログローテートはしてない。
  // ファイルサイズの大きさが気になったらの手動リセットする。
  loguru::init(argc, argv);
  loguru::add_file("everything.log", loguru::Truncate, loguru::Verbosity_MAX);

  // Detect interactive mode (if no arguments) and define UI session
  // 引数がない場合：対話モード
  G4UIExecutive *ui = nullptr;
  if (argc == 1)
  {
    ui = new G4UIExecutive(argc, argv);
  }

  // Optionally: choose a different Random engine...
  // （オプション）
  // 乱数のエンジンを変更できる
  // G4Random::setTheEngine(new CLHEP::MTwistEngine);

  // use G4SteppingVerboseWithUnits
  // ステッピングの精度（だと思う）
  G4int precision = 4;
  G4SteppingVerbose::UseBestUnit(precision);

  // Construct the default run manager
  // RunManagerの設定：
  // 最初はシングルスレッドで作成し、出力結果の整合性の確認＆デバッグするとよいらしい。
  // マルチスレッドはデバッグでハマることがあるらしい。
  // デバッグできたらマルチスレッドにする。
  // G4RunManagerType::Serial = シングルスレッド対応
  // G4RunManagerType::Default = マルチスレッド対応
  // auto rm = G4RunManagerFactory::CreateRunManager(G4RunManagerType::Serial);
  auto rm = G4RunManagerFactory::CreateRunManager(G4RunManagerType::Default);
  // rm->SetNumberOfThreads(1);
  LOG_F(INFO, "G4RunManagerFactory::CreateRunManager");

  // G4ScoringManager::GetScoringManager();

  LOG_F(INFO, "GetNumberOfThreads: %d", rm->GetNumberOfThreads());

  // Set mandatory initialization classes
  // 必須クラスの設定：
  // 1. detector
  // 2. physics
  // 3. actions

  // # 1. detector
  //
  // 1. 測定器の構築 -> DetectorConstruction.cc を参照
  // 2. 検出器の設定 -> TrackerSD.cc を参照
  // 3. ヒット情報の設定 -> TrackerHit.cc を参照
  //
  LOG_F(INFO, "new Geometry{}");
  auto geometry = new B2a::Geometry{};
  geometry->SetShieldMaterial("G4_AIR");
  geometry->SetShieldZ(5.0 * cm);
  LOG_F(INFO, "rm->Geometry");
  rm->SetUserInitialization(geometry);

  // # 2. physics
  //
  // Geant4デフォルトのFTFP_BERTモデルを使用
  // FTFP_BERTは Geant4.10 から標準になったモデル
  //
  LOG_F(INFO, "new FTFP_BERT{}");
  auto physics = new FTFP_BERT{};
  auto optical_physics = new G4OpticalPhysics{};
  physics->RegisterPhysics(optical_physics);
  // physics->RegisterPhysics(new G4StepLimiterPhysics());
  LOG_F(INFO, "rm->Physics");
  rm->SetUserInitialization(physics);

  // # 3. action
  //
  // 1. （オプション）ActionInitialization.cc を参照
  // 2. 入射粒子の設定 -> PrimaryGeneratorAction.cc を参照
  // 3. 保存ファイルの設定 -> RunAction.cc を参照
  // 4. 保存するイベントの設定 -> EventAction.cc を参照
  //
  LOG_F(INFO, "new ActionInitialization{}");
  auto actions = new B2::ActionInitialization{};
  LOG_F(INFO, "rm->Actions");
  rm->SetUserInitialization(actions);

  // 測定器の設定を完了
  // Initialize() したあとは、測定器を変更できない
  LOG_F(INFO, "rm->Initialize");
  rm->Initialize();
  // この変更は反映されない
  // detector->SetShieldZ(10 * cm);
  //

  // Initialize visualization with the default graphics system
  // 描画システムを設定
  auto vm = new G4VisExecutive(argc, argv);
  // Constructors can also take optional arguments:
  // - a graphics system of choice, eg. "OGL"
  // - and a verbosity argument - see /vis/verbose guidance.
  // auto visManager = new G4VisExecutive(argc, argv, "OGL", "Quiet");
  // auto visManager = new G4VisExecutive("Quiet");
  vm->Initialize();

  // Get the pointer to the User Interface manager
  auto um = G4UImanager::GetUIpointer();

  // Process macro or start UI session
  // 引数がない場合：対話モード
  // 引数がひとつ：マクロファイルとして読み込みバッチ処理
  //
  //
  if (!ui)
  {
    // batch mode
    G4String command = "/control/execute ";
    G4String fileName = argv[1];
    um->ApplyCommand(command + fileName);
  }
  else
  {
    // interactive mode
    um->ApplyCommand("/control/execute setup.mac");
    if (ui->IsGUI())
    {
      um->ApplyCommand("/control/execute gui.mac");
    }
    ui->SessionStart();
    delete ui;
  }

  // Job termination
  // 各種マネージャーの削除
  LOG_F(INFO, "delete vm");
  delete vm;
  LOG_F(INFO, "delete rm");
  delete rm;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
