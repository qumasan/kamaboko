//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file B2/B2a/include/TrackerHit.hh
/// \brief Definition of the B2::TrackerHit class

#ifndef B2TrackerHit_h
#define B2TrackerHit_h 1

#include "G4Allocator.hh"
#include "G4Step.hh"
#include "G4THitsCollection.hh"
#include "G4ThreeVector.hh"
#include "G4VHit.hh"
#include "tls.hh"

#include <map>

#include <loguru.hpp>

namespace B2
{

  /// Tracker hit class
  ///
  /// It defines data members to store the trackID, chamberNb, energy deposit,
  /// and position of charged particles in a selected volume:
  /// - fTrackID, fChamberNB, fEdep, fPos

  class TrackerHit : public G4VHit
  {
  public:
    TrackerHit();
    TrackerHit(const TrackerHit &) = default;
    ~TrackerHit() override = default;

    // operators: 演算子オーバーロード
    TrackerHit &operator=(const TrackerHit &) = default;
    G4bool operator==(const TrackerHit &) const;
    inline void *operator new(size_t);
    inline void operator delete(void *);

    // methods from base class
    void Draw() override;
    void Print() override;

    // カスタム
    void Fill(G4Step *aStep);
    G4String ToCsvString() const;
    G4String ToLtsvString() const;

    // あとで消す
    void SetChamberNb(G4int chamb) { fChamberNb = chamb; };
    G4int GetChamberNb() const { return fChamberNb; };

    void SetEdep(G4double de) { fEdep = de; };
    G4double GetEdep() const { return fEdep; };

    void SetPos(G4ThreeVector xyz) { fPos = xyz; };
    G4ThreeVector GetPos() const { return fPos; };

  private:
    // Fillした結果をstd::mapに変換するメソッド
    //
    // std::map は同一の key:value 値しか格納できないため
    // 変数の型に応じたmapを作成した
    //
    // - fHitInt: G4int型のデータを格納する
    // - fHitDouble: G4double型のデータを格納する
    // - fHitString: G4String型のデータを格納する変数
    //
    // C++17以降は、std::variantやstd::anyを使って、
    // 任意の型を組み合わせることができるらしいが、
    // 型の安全性を優先して、型別にした

    void ToMap();
    std::map<std::string, G4int> fHitInt{};
    std::map<std::string, G4double> fHitDouble{};
    std::map<std::string, G4String> fHitString{};

  private:
    // シミュレーション結果を格納するメンバー変数
    //
    // 以下のような命名規則で変数名を作成した
    // - fStep*: G4Step or G4StepPoint から得られる値
    // - fTrack*: G4Track から得られる値
    // - fVertex*: 入射粒子から得られる値
    // 新規に変数を追加する場合も
    // なるべくこの命名規則を守るようにする

    // IDs
    G4int fRunID{-1};         // G4Run
    G4int fEventID{-1};       // G4Event
    G4int fTrackID{-1};       // G4Track
    G4int fStepID{-1};        // G4Track
    G4int fTrackParentID{-1}; // G4Track

    // G4Step
    G4double fEnergyDeposit{0.};

    // G4StepPoint (PreStepPoint)
    G4ThreeVector fStepXYZ{};
    G4double fStepGlobalTime{0.};
    G4double fStepTotalEnergy{0.};
    G4String fStepMaterialName{};

    // G4Track
    G4double fTrackLength{0.};
    G4double fStepLength{0.};
    G4ThreeVector fTrackXYZ{};
    G4double fTrackTotalEnergy{0.};
    G4double fTrackKineticEnergy{0.};
    G4ThreeVector fTrackMomentum{};
    G4ThreeVector fTrackMomentumDirection{};
    G4ThreeVector fVertexXYZ{};
    G4double fVertexTotalEnergy{0.};
    G4double fVertexKineticEnergy{0.};
    G4ThreeVector fVertexMomentumDirection{0.};

    // G4TouchableHistory
    G4int fDetectorID{-1};
    G4double fSolidX{0.};
    G4double fSolidY{0.};
    G4double fSolidZ{0.};
    G4int fCopyID{-1};
    G4int fReplicaID{-1};
    G4int fHistoryDepth{-1};
    G4String fLVName{};
    G4String fPVName{};
    G4String fSDName{};

    // G4ParticleDefinition
    G4String fParticleName{};
    G4int fParticleID{};

    // あとで消す
    G4int fChamberNb = -1;
    G4double fEdep = 0.;
    G4ThreeVector fPos;
  };

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  using TrackerHitsCollection = G4THitsCollection<TrackerHit>;

  extern G4ThreadLocal G4Allocator<TrackerHit> *TrackerHitAllocator;

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  inline void *TrackerHit::operator new(size_t)
  {
    // operator new:
    // メモリ割り当てのための特殊関数。
    // クラスごとにオーバーライドして、カスタマイズできる。
    // コンストラクターの前に実行される。
    // 1. TrackerHit::operator new
    // 2. TrackerHit::TrackerHit()
    VLOG_F(1, "TrackerHit::operator new");

    if (!TrackerHitAllocator)
      TrackerHitAllocator = new G4Allocator<TrackerHit>;
    return (void *)TrackerHitAllocator->MallocSingle();
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  inline void TrackerHit::operator delete(void *hit)
  {
    // operator delete:
    // operator new で割り当てられたメモリを解放するための特殊関数。
    // クラスごとにオーバーライドしてカスタマイズできる。
    // デストラクターの後に実行される。
    // 1. TrackerHit::~TrackerHit()
    // 2. TrackerHit::operator delete
    VLOG_F(1, "TrackerHit::operator delete");
    TrackerHitAllocator->FreeSingle((TrackerHit *)hit);
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

}

#endif
