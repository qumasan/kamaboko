/// \file B2/B2a/include/PrimaryGeneratorAction.hh
/// \brief Definition of the B2::PrimaryGeneratorAction class

#ifndef B2PrimaryGenerator_h
#define B2PrimaryGenerator_h 1

#include "G4PrimaryVertex.hh"
#include "G4VUserPrimaryGeneratorAction.hh"
// #include "globals.hh"

class G4ParticleGun;
class G4Event;

namespace B2
{

  /// The primary generator action class with particle gun.
  ///
  /// It defines a single particle which hits the Tracker
  /// perpendicular to the input face. The type of the particle
  /// can be changed via the G4 build-in commands of G4ParticleGun class
  /// (see the macros provided with this example).

  class PrimaryGenerator : public G4VUserPrimaryGeneratorAction
  {
  public:
    PrimaryGenerator();
    ~PrimaryGenerator() override;

    void GeneratePrimaries(G4Event *aEvent) override;

    G4ParticleGun *GetParticleGun() { return fParticleGun; }

  private:
    G4ParticleGun *fParticleGun = nullptr; // G4 particle gun

  private:
    G4ParticleGun *SetupGunMuon();
  };
}

#endif
