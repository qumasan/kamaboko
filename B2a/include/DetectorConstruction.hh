/// \file B2/B2a/include/DetectorConstruction.hh
/// \brief Definition of the B2a::DetectorConstruction class

#ifndef B2aDetectorConstruction_h
#define B2aDetectorConstruction_h 1

#include "G4SystemOfUnits.hh"
#include "G4VUserDetectorConstruction.hh"
// #include "globals.hh"
// #include "tls.hh"

class G4VPhysicalVolume;
class G4LogicalVolume;
class G4Material;
class G4UserLimits;
class G4GlobalMagFieldMessenger;

/**
 * # DetectorConstructionクラス
 *
 * G4VUserDetectorConstructionの派生クラスです。
 * このクラスの中で測定器を定義します。
 * main()関数で DetectorConstruction のオブジェクトを作成すると、
 * DetectorConstruction::Construct() が呼ばれ、測定器が構築されます。
 * その後で DetectorConstruction::ConstructSDandField() が呼ばれ、
 * 検出する範囲を指定できます。
 *
 * ```cpp
 * int main()
 * {
 *     auto rm = G4RunManagerFactory::CreateRunManager();
 *     auto detector = new B2a::DetectorConstruction{};
 *     rm->SetUserInitialization(detector);
 *     // 省略
 *     return 0;
 * }
 * ```
 */
namespace B2a
{

  class DetectorMessenger;

  /// Detector construction class to define materials, geometry
  /// and global uniform magnetic field.

  class DetectorConstruction : public G4VUserDetectorConstruction
  {
  public:
    DetectorConstruction();
    ~DetectorConstruction() override;

  public:
    /**
     * @brief すべての測定器をセットアップ
     *
     * Geant4シミュレーションで使う測定器をセットアップするメソッドです。
     * 純粋仮想関数になっているためユーザー実装が必須です。
     */
    G4VPhysicalVolume *Construct() override;

    /**
     * @brief SensitiveDetectorをセットアップ
     *
     * データ取得に必要なSensitiveDetectorをセットアップするメソッドです。
     * ユーザー実装はオプションですが、実装したほうがよいです。
     * 基底クラスから継承したメソッドを使って論理ボリュームに設定するだけです。
     * （G4VUserDetectorConstruction::SetSensitiveDetector）
     */
    void ConstructSDandField() override;

  public:
    /**
     * @brief 四角柱の論理ボリュームを作成
     *
     * 四角柱（G4Box型）の論理ボリュームを作成する汎用メソッドです。
     * サイズは「全長」で設定してください。
     *
     * @param logical_name: 論理ボリュームの名前
     * @param material_name: 材料の名前
     * @param full_length_x: 四角柱の全長（x方向）
     * @param full_length_y: 四角柱の全長（y方向）
     * @param full_length_z: 四角柱の全長（z方向）
     *
     * @code
     * auto waterBox = SetupLogicalBox("Box", "G4_WATER", 10. * cm, 5. * cm, 1. * cm);
     * @endcode
     */
    G4LogicalVolume *SetupLogicalBox(
        const G4String &logical_name,
        const G4String &material_name,
        const G4double full_length_x,
        const G4double full_length_y,
        const G4double full_length_z);

  private:
    /**
     * @brief ワールド（World）をセットアップ
     *
     * Geant4の空間（World）を作成するメソッドです。
     * 内部で SetupLogicalBox を使っています。
     *
     * @param fWorldLVName: ワールドボリューム名前{"World"}
     * @param fWorldMaterial: Worldの材質{"G4_AIR"}
     * @param fWorldX Worldの全長（x方向）
     * @param fWorldY Worldの全長（y方向）
     * @param fWorldZ Worldの全長（z方向）
     *
     * @code
     * auto world = SetupWorldVolume();
     * @endcode
     */
    G4LogicalVolume *SetupWorldVolume();
    G4String fWorldLVName{"World"};
    G4String fWorldMaterial{"G4_AIR"};
    G4double fWorldX = 30.0 * cm;
    G4double fWorldY = 30.0 * cm;
    G4double fWorldZ = 30.0 * cm;

  private:
    /**
     * @brief 遮蔽物をセットアップ
     *
     * OSECHIの前方に設置する遮蔽物を作成するメソッドです。
     * 内部で SetupLogicalBox を使っています。
     *
     * @param fShieldLVName: ボリューム名{"Shield"}
     * @param fShieldMaterial: 遮蔽物の材質{"G4_Pb"}
     * @param fShieldX: 遮蔽物の全長（x方向）
     * @param fShieldY: 遮蔽物の全長（y方向）
     * @param fShieldZ: 遮蔽物の全長（z方向）
     *
     * @code
     * auto shield = SetupShieldVolume();
     * @endcode
     */
    G4LogicalVolume *SetupShieldVolume();
    G4String fShieldLVName{"Shield"};
    G4String fShieldMaterial{"G4_Pb"};
    G4double fShieldX = 20.0 * cm;
    G4double fShieldY = 20.0 * cm;
    G4double fShieldZ = 0.5 * cm;

  public:
    /**
     * @brief 遮蔽物のパラメータを確認／変更
     *
     * 遮蔽物のパラメータをmain関数から確認／変更するためのメソッドです。
     *
     * @code
     * auto rm = G4RunManagerFactory::CreateRunManager();
     * auto detector = new DetectorConstruction{};
     * detector->SetShieldMaterial("G4_WATER");  // 水に変更
     * rm->SetUserInitialization(detector);
     * @endcode
     */
    G4String GetShieldLVName() const { return fShieldLVName; };
    void SetShieldLVName(const G4String &name) { fShieldLVName = name; };

    G4String GetShieldMaterial() const { return fShieldMaterial; };
    void SetShieldMaterial(const G4String &name) { fShieldMaterial = name; };

    G4double GetShieldX() const { return fShieldX; };
    void SetShieldX(const G4double length) { fShieldX = length; };

    G4double GetShieldY() const { return fShieldY; };
    void SetShieldY(const G4double length) { fShieldY = length; };

    G4double GetShieldZ() const { return fShieldZ; };
    void SetShieldZ(const G4double length) { fShieldZ = length; };

  private:
    /**
     * @brief シンチレーター（1枚）をセットアップ
     *
     * OSECHIで使用するシンチレーターを1枚作成するメソッドです。
     * 内部で SetupLogicalBox を使っています。
     *
     * @param fLayerLVName: ボリューム名{"Shield"}
     * @param fLayerMaterial: シンチレーターの材質{"G4_PLASTIC_SC_VINYLTOLUENE"}
     * @param fLayerX: シンチレーターの全長（x方向）
     * @param fLayerY: シンチレーターの全長（y方向）
     * @param fLayerZ: シンチレーターの全長（z方向）
     */
    G4LogicalVolume *SetupLayerVolume();
    G4String fLayerLVName{"Layer"};
    G4String fLayerMaterial{"G4_PLASTIC_SC_VINYLTOLUENE"};
    G4double fLayerX = 10.0 * cm;
    G4double fLayerY = 5.0 * cm;
    G4double fLayerZ = 1.0 * cm;

  public:
    /**
     * @brief シンチレーターのパラメータを確認／変更
     *
     * シンチレーター（1枚）のパラメータをmain関数から確認／変更するためのメソッドです。
     *
     * @code
     * auto rm = G4RunManagerFactory::CreateRunManager();
     * auto detector = new DetectorConstruction{};
     * detector->SetLayerZ(10.0 * cm);  // 厚さを変更
     * rm->SetUserInitialization(detector);
     * @endcode
     */
    G4String GetLayerLVName() const { return fLayerLVName; };
    void SetLayerLVName(const G4String &name) { fLayerLVName = name; };

    G4String GetLayerMaterial() const { return fLayerMaterial; };
    void SetLayerMaterial(const G4String &name) { fLayerMaterial = name; };

    G4double GetLayerX() const { return fLayerX; };
    void SetLayerX(const G4double length) { fLayerX = length; };

    G4double GetLayerY() const { return fLayerY; };
    void SetLayerY(const G4double length) { fLayerY = length; };

    G4double GetLayerZ() const { return fLayerZ; };
    void SetLayerZ(const G4double length) { fLayerZ = length; };

  private:
    G4LogicalVolume *SetupOsechi(G4int n_elements);

  private:
    G4VPhysicalVolume *SetupVolumes();

    // TODO: DefineVolumesの中身を分割する
    // TODO: G4LogicalVolume *DefineOsechi() : シンチレーター3枚を作成する
    // TODO: DefineVolumesの中で、これらを受け取り物理ボリュームに配置する

    // ここから下は B2a のサンプルにもとから存在したメソッド
    // 不要なものはだんだん削除する
  public:
    // Set methods
    void SetTargetMaterial(G4String);
    void SetChamberMaterial(G4String);
    void SetMaxStep(G4double);
    void SetCheckOverlaps(G4bool);

  private:
    // methods
    void DefineMaterials();
    G4VPhysicalVolume *DefineVolumes();

    // static data members
    static G4ThreadLocal G4GlobalMagFieldMessenger *fMagFieldMessenger;
    // magnetic field messenger
    // data members

    // 検出器の数
    // TODO: Chamberの作り方を複製して、OSECHIを作成する

    G4int fNbOfChambers = 0;
    // TODO: fNumberOfLayers = 0;

    // 論理ボリューム
    G4LogicalVolume *fLogicTarget = nullptr;   // pointer to the logical Target
    G4LogicalVolume **fLogicChamber = nullptr; // pointer to the logical Chamber
    // TODO: fLogicLayers **fLogicLayer = nullptr;

    // マテリアル
    G4Material *fTargetMaterial = nullptr;  // pointer to the target  material
    G4Material *fChamberMaterial = nullptr; // pointer to the chamber material
    // TODO: fLayerMaterial = nullptr;

    G4UserLimits *fStepLimit = nullptr; // pointer to user step limits

    DetectorMessenger *fMessenger = nullptr; // messenger

    G4bool fCheckOverlaps = true; // option to activate checking of volumes overlaps
  };

}

#endif
