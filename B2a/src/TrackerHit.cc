//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file B2/B2a/src/TrackerHit.cc
/// \brief Implementation of the B2::TrackerHit class

#include "TrackerHit.hh"
#include "G4Box.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4EventManager.hh"
#include "G4Run.hh"
#include "G4RunManagerFactory.hh"
#include "G4Step.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4VVisManager.hh"
#include "G4VisAttributes.hh"

#include <iomanip>
#include <loguru.hpp>

namespace B2
{

  G4ThreadLocal G4Allocator<TrackerHit> *TrackerHitAllocator = nullptr;

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
  TrackerHit::TrackerHit()
  {
    VLOG_F(1, "TrackerHit()");
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  G4bool TrackerHit::operator==(const TrackerHit &right) const
  {
    return (this == &right) ? true : false;
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  void TrackerHit::Draw()
  {
    G4VVisManager *pVVisManager = G4VVisManager::GetConcreteInstance();
    if (pVVisManager)
    {
      G4Circle circle(fPos);
      circle.SetScreenSize(100.);
      circle.SetFillStyle(G4Circle::filled);
      G4VisAttributes attribs(G4Colour::Green());
      circle.SetVisAttributes(attribs);
      pVVisManager->Draw(circle);
    }
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  void TrackerHit::Print()
  {
    VLOG_SCOPE_F(1, "Print()");
    VLOG_F(2, "fRunID: %d", fRunID);
    VLOG_F(2, "fEventID: %d", fEventID);
    VLOG_F(2, "fEnergyDeposit: %f", fEnergyDeposit);
    VLOG_F(2, "fStepXYZ: (%f, %f, %f)", fStepXYZ.getX(), fStepXYZ.getY(), fStepXYZ.getZ());
    VLOG_F(2, "fStepGlobalTime: %f", fStepGlobalTime);
    VLOG_F(2, "fStepTotalEnergy: %f", fStepTotalEnergy);
    VLOG_F(2, "fTrackID: %d", fTrackID);
    VLOG_F(2, "fStepID: %d", fStepID);
    VLOG_F(2, "fTrackLength: %f", fTrackLength);
    VLOG_F(2, "fStepLength: %f", fStepLength);

    VLOG_F(2, "fDetectorID: %d", fDetectorID);
    VLOG_F(2, "fCopyID: %d", fCopyID);
    VLOG_F(2, "fReplicaID: %d", fReplicaID);
    VLOG_F(2, "fHistoryDepth: %d", fHistoryDepth);
    VLOG_F(2, "fLVName: %s", fLVName.c_str());
    VLOG_F(2, "fPVName: %s", fPVName.c_str());

    VLOG_F(2, "fParticleName: %s", fParticleName.c_str());
    VLOG_F(2, "fParticleID: %d", fParticleID);
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  // __________________________________________________
  void TrackerHit::Fill(G4Step *aStep)
  {
    VLOG_F(1, "Fill()");

    // G4Run
    const G4Run *run = G4RunManagerFactory::GetMasterRunManager()->GetCurrentRun();
    fRunID = run->GetRunID();

    // G4Event
    const G4Event *event = G4EventManager::GetEventManager()->GetConstCurrentEvent();
    fEventID = event->GetEventID();

    // G4Step
    fEnergyDeposit = aStep->GetTotalEnergyDeposit() / MeV;

    // G4StepPoint
    auto pre_step = aStep->GetPreStepPoint();
    fStepXYZ = pre_step->GetPosition() / mm;
    fStepGlobalTime = pre_step->GetGlobalTime() / ns;
    fStepTotalEnergy = pre_step->GetTotalEnergy() / MeV;
    fStepMaterialName = pre_step->GetMaterial()->GetName();
    // pre_step->GetKineticEnergy()
    // pre_step->GetMomentum();
    // pre_step->GetMomentumDirection();

    // G4Track
    auto track = aStep->GetTrack();
    fTrackID = track->GetTrackID();
    fTrackParentID = track->GetParentID();
    fStepID = track->GetCurrentStepNumber();
    fTrackLength = track->GetTrackLength() / mm;
    fStepLength = track->GetStepLength() / mm;
    fTrackXYZ = track->GetPosition() / mm;
    fTrackTotalEnergy = track->GetTotalEnergy() / MeV;
    fTrackKineticEnergy = track->GetKineticEnergy() / MeV;
    fTrackMomentum = track->GetMomentum() / MeV;
    fTrackMomentumDirection = track->GetMomentumDirection();
    fVertexXYZ = track->GetVertexPosition() / mm;
    fVertexKineticEnergy = track->GetVertexKineticEnergy();
    fVertexMomentumDirection = track->GetVertexMomentumDirection();

    // G4TouchableHistory
    auto touch = pre_step->GetTouchableHandle();
    fCopyID = touch->GetCopyNumber();
    fReplicaID = touch->GetReplicaNumber();
    fHistoryDepth = touch->GetHistoryDepth();
    auto solid = touch->GetSolid();
    if (G4Box *box = dynamic_cast<G4Box *>(solid))
    {
      fSolidX = box->GetXHalfLength() * 2. / mm;
      fSolidY = box->GetYHalfLength() * 2. / mm;
      fSolidZ = box->GetZHalfLength() * 2. / mm;
    }

    // G4VPhysicalVolume
    auto pv = pre_step->GetPhysicalVolume();
    fDetectorID = pv->GetCopyNo();
    fPVName = pv->GetName();

    // G4LogicalVolume
    auto lv = pv->GetLogicalVolume();
    fLVName = lv->GetName();

    // G4ParticleDefinition
    auto particle = track->GetParticleDefinition();
    fParticleName = particle->GetParticleName();
    fParticleID = particle->GetPDGEncoding();

    // std::map型に代入
    ToMap();
  };

  // __________________________________________________
  G4String TrackerHit::ToCsvString() const
  {
    // ヒット用mapからCSV形式の文字列を生成する
    VLOG_F(1, "ToCsvString()");

    std::stringstream ss;

    G4bool is_first = true;
    for (const auto &pair : fHitInt)
    {
      if (!is_first)
      {
        ss << ",";
      }
      ss << pair.second;
      is_first = false;
    };

    for (const auto &pair : fHitDouble)
    {
      ss << "," << pair.second;
    };

    for (const auto &pair : fHitString)
    {
      ss << "," << pair.second;
    };

    G4String csv{ss.str()};

    return csv;
  };

  // __________________________________________________
  G4String TrackerHit::ToLtsvString() const
  {
    // ヒット用mapからLTSV形式の文字列を生成する
    // - LTSV (Labeled Tab-Separated Value)と書いたが、区切り文字はカンマを使っている
    // - 出力したファイルにラベルを追加することで、解析ツールに汎用性を持たせることができる
    // - カラムの順番がファイルから取得できる

    VLOG_SCOPE_F(1, "ToLtsvString()");

    std::stringstream ss;

    G4bool is_first = true;
    for (const auto &pair : fHitInt)
    {
      if (!is_first)
      {
        ss << ",";
      }
      ss << pair.first << ":" << pair.second;
      is_first = false;
    };

    for (const auto &pair : fHitDouble)
    {
      ss << "," << pair.first << ":" << pair.second;
    };

    for (const auto &pair : fHitString)
    {
      ss << "," << pair.first << ":" << pair.second;
    };

    G4String ltsv{ss.str()};

    VLOG_F(5, "ToLtsvString(): ltsv = %s", ltsv.c_str());

    return ltsv;
  };

  // __________________________________________________
  void TrackerHit::ToMap()
  {
    VLOG_F(1, "ToMap()");

    // int型のデータ
    fHitInt["run_id"] = fRunID;
    fHitInt["event_id"] = fEventID;
    fHitInt["track_id"] = fTrackID;
    fHitInt["step_id"] = fStepID;
    fHitInt["parent_id"] = fTrackParentID;
    fHitInt["detector_id"] = fDetectorID;
    fHitInt["copy_id"] = fCopyID;
    fHitInt["replica_id"] = fReplicaID;
    fHitInt["history_depth"] = fHistoryDepth;
    fHitInt["particle_id"] = fParticleID;

    // double型のデータ
    fHitDouble["energy_deposit"] = fEnergyDeposit / MeV;
    fHitDouble["step_x"] = fStepXYZ.getX() / mm;
    fHitDouble["step_y"] = fStepXYZ.getY() / mm;
    fHitDouble["step_z"] = fStepXYZ.getZ() / mm;
    fHitDouble["step_time"] = fStepGlobalTime / ns;
    fHitDouble["step_total_energy"] = fStepTotalEnergy / MeV;
    fHitDouble["track_length"] = fTrackLength / mm;
    fHitDouble["step_length"] = fStepLength / mm;
    fHitDouble["track_x"] = fTrackXYZ.getX() / mm;
    fHitDouble["track_y"] = fTrackXYZ.getY() / mm;
    fHitDouble["track_z"] = fTrackXYZ.getZ() / mm;
    fHitDouble["track_total_energy"] = fTrackTotalEnergy / MeV;
    fHitDouble["track_kinetic_energy"] = fTrackKineticEnergy / MeV;
    fHitDouble["track_momentum_x"] = fTrackMomentum.getX() / MeV;
    fHitDouble["track_momentum_y"] = fTrackMomentum.getY() / MeV;
    fHitDouble["track_momentum_z"] = fTrackMomentum.getZ() / MeV;
    fHitDouble["track_momentum_direction_x"] = fTrackMomentumDirection.getX() / MeV;
    fHitDouble["track_momentum_direction_y"] = fTrackMomentumDirection.getY() / MeV;
    fHitDouble["track_momentum_direction_z"] = fTrackMomentumDirection.getZ() / MeV;
    fHitDouble["vertex_x"] = fVertexXYZ.getX() / mm;
    fHitDouble["vertex_y"] = fVertexXYZ.getY() / mm;
    fHitDouble["vertex_z"] = fVertexXYZ.getZ() / mm;
    fHitDouble["vertex_total_energy"] = fVertexTotalEnergy / MeV;
    fHitDouble["vertex_kinetic_energy"] = fVertexKineticEnergy / MeV;
    fHitDouble["vertex_momentum_direction_x"] = fVertexMomentumDirection.getX() / MeV;
    fHitDouble["vertex_momentum_direction_y"] = fVertexMomentumDirection.getY() / MeV;
    fHitDouble["vertex_momentum_direction_z"] = fVertexMomentumDirection.getZ() / MeV;
    fHitDouble["solid_x"] = fSolidX / mm;
    fHitDouble["solid_y"] = fSolidY / mm;
    fHitDouble["solid_z"] = fSolidZ / mm;

    // str型のデータ
    fHitString["step_material_name"] = fStepMaterialName;
    fHitString["lv_name"] = fLVName;
    fHitString["pv_name"] = fPVName;
    fHitString["sd_name"] = fSDName;
    fHitString["particle_name"] = fParticleName;
  };

};
