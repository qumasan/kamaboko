/// \file B2/B2a/src/TrackerSD.cc
/// \brief Implementation of the B2::TrackerSD class

#include "TrackerSD.hh"

#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4Run.hh"
#include "G4RunManagerFactory.hh"
#include "G4SDManager.hh"
#include "G4Step.hh"
#include "G4SystemOfUnits.hh"
#include "G4ThreeVector.hh"
#include "G4UnitsTable.hh"
#include "G4ios.hh"

#include <chrono>  // std::chrono::system_clock
#include <ctime>   // std::time_t, std::tm, localtime_r
#include <fstream> // std::fstream
#include <iomanip> // std::put_time
#include <sstream> // std::stringstream

#include <loguru.hpp>

namespace
{
  // 無名の名前空間に入れて、スコープを小さくする
  /**
   * @fn
   * 時刻付きのファイル名を生成するローカル関数
   * @params (aPrefix) ファイル名のプリフィックス
   * @params (aSuffix) ファイル名のサフィックス
   * @params (aSaveTo) ファイルを保存するディレクトリ名
   * @return 時刻付きのファイル名 {aSaveTo}/{aPrefix}_{yyyy-mm-ddTHHhMMmSSs}.{aSuffix}
   */
  G4String getFileName(
      const G4String &aPrefix,
      const G4String &aSuffix,
      const G4String &aFileType = "csv",
      const G4String &aSaveTo = ".")
  {
    // 現在のシステム時刻を取得
    auto now = std::chrono::system_clock::now();
    // time_t形式に変換
    std::time_t now_t = std::chrono::system_clock::to_time_t(now);
    // tm構造体に変換
    std::tm now_tm;
    localtime_r(&now_t, &now_tm);

    // 時刻をフォーマット
    std::stringstream ymd;
    // ymd << std::put_time(&now_tm, "%Y-%m-%dT%Hh%Mm%Ss");
    ymd << std::put_time(&now_tm, "%Y-%m-%dT%Hh%Mm");

    // ファイル名を生成
    // fileName = "{aSaveTo}/{aPrefix}_{yyyy-mm-ddTHHhMMmSSs}.{aSuffix}"
    std::stringstream fileName;
    // fileName << aSaveTo << "/" << aPrefix << "_" << ymd.str() << "." << aSuffix;
    fileName << aSaveTo << "/" << aPrefix << "_" << ymd.str() << "_" << aSuffix << "." << aFileType;
    return fileName.str();
  }
}

namespace B2
{

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  TrackerSD::TrackerSD(const G4String &name, const G4String &hitsCollectionName)
      : G4VSensitiveDetector(name)
  {
    VLOG_SCOPE_F(1, "TrackerSD()");
    VLOG_F(2, "SDname: %s", name.c_str());
    VLOG_F(2, "hitsCollectionName: %s", hitsCollectionName.c_str());

    collectionName.insert(hitsCollectionName);
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  void TrackerSD::Initialize(G4HCofThisEvent *hce)
  {
    // LOG_F(INFO, "GetNumberOfThreads: %d", rm->GetNumberOfThreads());
    VLOG_SCOPE_F(1, "Initialize()");

    // TrackerHitsCollectionのヒット配列を準備する
    fHitsCollection = new TrackerHitsCollection{
        SensitiveDetectorName, // "TrackerChamberSD"
        collectionName[0]      // "TrackerHitsCollection"
    };
    // LOG_F(INFO, "SensitiveDetectorName: %s", SensitiveDetectorName.c_str());
    // LOG_F(INFO, "CollectionName[0]: %s", collectionName[0].c_str());

    // SD設定の確認
    auto sm = G4SDManager::GetSDMpointer();
    sm->ListTree();

    G4int n_tables = sm->GetHCtable()->entries();
    VLOG_F(2, "HCtables=%d", n_tables);

    for (G4int i = 0; i < n_tables; i++)
    {
      G4String hc_name = sm->GetHCtable()->GetHCname(i);
      VLOG_F(3, "HCtable=%s", hc_name.c_str());
    };

    // Add this collection in hce

    // G4int hcID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
    // LOG_F(INFO, "hcID: %d", hcID);
    // hce->AddHitsCollection(hcID, fHitsCollection);
  };

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  G4bool TrackerSD::ProcessHits(G4Step *aStep, G4TouchableHistory *)
  {
    VLOG_SCOPE_F(1, "ProcessHits()");

    G4StepPoint *pre_step = aStep->GetPreStepPoint();
    auto pv = pre_step->GetPhysicalVolume();
    auto lv = pv->GetLogicalVolume();

    auto post_step = aStep->GetPostStepPoint();
    auto post_pv = post_step->GetPhysicalVolume();

    G4Track *track = aStep->GetTrack();
    auto particle = track->GetParticleDefinition();

    // エネルギー損失がない場合はスキップする
    G4double edep = aStep->GetTotalEnergyDeposit();
    if (edep == 0.)
      return false;

    auto newHit = new TrackerHit();
    newHit->SetChamberNb(aStep->GetPreStepPoint()->GetTouchableHandle()->GetCopyNumber());
    newHit->SetEdep(edep);
    newHit->SetPos(aStep->GetPostStepPoint()->GetPosition());

    newHit->Fill(aStep);
    newHit->Print();

    fHitsCollection->insert(newHit);

    // newHit->Print();

    return true;
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  void TrackerSD::EndOfEvent(G4HCofThisEvent * /*aHCE*/)
  {
    VLOG_SCOPE_F(1, "EndOfEvent");
    VLOG_F(2, "verboseLevel: %d", verboseLevel);

    // ヒット配列の確認
    VLOG_F(2, "HCname=%s", fHitsCollection->GetName().c_str());
    VLOG_F(2, "SDname=%s", fHitsCollection->GetSDname().c_str());
    G4int n_collections = fHitsCollection->entries();
    VLOG_F(2, "n_collections: %d", n_collections);

    // ヒット配列をファイルに保存する
    // EndOfEventが呼ばれた時刻をファイル名につける
    // 同じイベントのヒットはひとつのファイルにまとめる
    const G4Run *run = G4RunManagerFactory::GetMasterRunManager()->GetCurrentRun();
    G4int run_id = run->GetRunID();
    const G4Event *event = G4EventManager::GetEventManager()->GetConstCurrentEvent();
    G4int event_id = event->GetEventID();
    std::stringstream prefix;
    prefix << "kamaboko_ltsv";

    std::stringstream suffix;
    suffix << "r" << std::setfill('0') << std::setw(5) << run_id;
    suffix << "e" << std::setfill('0') << std::setw(5) << event_id;

    auto file_name = getFileName(
        prefix.str(),
        suffix.str(),
        "csv",
        "mc_data");
    std::ofstream file(file_name.c_str(), std::ios::app);
    if (!file.is_open())
    {
      LOG_F(WARNING, "Failed to open file: %s", file_name.c_str());
      return;
    }
    G4cout << "File opened: " << file_name << G4endl;
    VLOG_F(1, "File opened: %s", file_name.c_str());

    for (G4int i = 0; i < n_collections; i++)
    {
      auto h = (TrackerHit *)fHitsCollection->GetHit(i);
      VLOG_F(2, "[Hit] %s", h->ToLtsvString().c_str());
      file << h->ToLtsvString() << "\n";
    };
    file.close();

    G4cout << "File closed: " << file_name << G4endl;
    VLOG_F(1, "File closed: %s", file_name.c_str());

    // イベントが終了したらメモリを解放する
    delete fHitsCollection;

    if (verboseLevel > 1)
    {
      std::size_t entries = fHitsCollection->entries();
      G4cout
          << G4endl
          << "-------->Hits Collection: in this event they are "
          << entries
          << " hits in the tracker chambers: "
          << G4endl;

      for (std::size_t i = 0; i < entries; i++)
      {
        (*fHitsCollection)[i]->Print();
      };
    };
  };

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

}
