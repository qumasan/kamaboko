/// \file B2/B2a/src/PrimaryGenerator.cc
/// \brief Implementation of the B2::PrimaryGeneratorAction class

#include "PrimaryGenerator.hh"

#include "G4Box.hh"
#include "G4Event.hh"
#include "G4LogicalVolume.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"

#include <loguru.hpp>
namespace B2
{

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  PrimaryGenerator::PrimaryGenerator()
  {
    LOG_F(INFO, "PrimaryGenerator()");
    // 1イベントに入射する粒子数: 1個
    fParticleGun = SetupGunMuon();
  };

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  PrimaryGenerator::~PrimaryGenerator()
  {
    LOG_F(INFO, "~PrimaryGenerator()");
    delete fParticleGun;
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  void PrimaryGenerator::GeneratePrimaries(G4Event *aEvent)
  {
    VLOG_SCOPE_F(1, "GeneratePrimaries(%d)", aEvent->GetEventID());
    // In order to avoid dependence of PrimaryGeneratorAction
    // on DetectorConstruction class we get world volume
    // from G4LogicalVolumeStore.

    // この関数は、イベント開始するときに呼ばれる

    // すでにセットアップされている論理ボリュームにアクセス
    // ここからでは、DetectorConstructionの中身がみえないので
    // G4LogicalVolumeStore::GetInstance()を介して取ってきている。
    //
    // PrimaryGeneratorActionがDetectorConstructionに依存しないように、
    // と書いてはいるが、論理ボリューム名（"World"）はハードコードしている。
    // Worldボリュームの名前を変更すると、この計算はできなくなる

    G4LogicalVolume *worldLV = G4LogicalVolumeStore::GetInstance()->GetVolume("World");

    G4double worldZHalfLength = 0;
    G4Box *worldBox = nullptr;
    if (worldLV)
      worldBox = dynamic_cast<G4Box *>(worldLV->GetSolid());
    if (worldBox)
      worldZHalfLength = worldBox->GetZHalfLength();
    else
    {
      G4cerr << "World volume of box not found." << G4endl;
      G4cerr << "Perhaps you have changed geometry." << G4endl;
      G4cerr << "The gun will be place in the center." << G4endl;
    }

    // Starting a primary particle close to the world boundary.
    // ワールドの境界から少し内側（1um）に入射点を配置する
    // G4ThreeVector position = G4ThreeVector{0., 0., -worldZHalfLength + 1 * um};
    // fParticleGun->SetParticlePosition(position);

    // G4Event に関連づける
    // G4ParticleGun に G4Eventを紐づける方法と
    // G4Eventに G4PrimaryVertex を追加する方法があるみたい
    fParticleGun = SetupGunMuon();
    fParticleGun->GeneratePrimaryVertex(aEvent);
  }

  // __________________________________________________
  G4ParticleGun *PrimaryGenerator::SetupGunMuon()
  {
    VLOG_SCOPE_F(2, "SetupGunMuon()");
    // ParticleGunを作成
    G4int n_particles = 1;
    G4ParticleGun *gun = new G4ParticleGun{n_particles};

    // ParticleTableから mu- を取得
    G4ParticleTable *table = G4ParticleTable::GetParticleTable();
    auto particle = table->FindParticle("mu-");
    gun->SetParticleDefinition(particle);

    // 入射エネルギーと方向を定義
    G4double energy = 1.0 * GeV;
    G4ThreeVector direction = G4ThreeVector{0., 0., 1.};
    gun->SetParticleEnergy(energy);
    gun->SetParticleMomentumDirection(direction);

    // 入射位置の設定
    G4double x = 20.0 * cm * (G4UniformRand() - 0.5);
    G4double y = 20.0 * cm * (G4UniformRand() - 0.5);
    G4double z = -95.0 * cm;
    G4ThreeVector v0 = G4ThreeVector{x, y, z};
    G4double t0 = 0.0 * ns;
    auto vertex = new G4PrimaryVertex{v0, t0};
    gun->SetParticlePosition(v0);
    gun->SetParticleTime(t0);

    //
    VLOG_F(2, "Particle=%s", particle->GetParticleName().c_str());
    VLOG_F(2, "Energy=%f [MeV]", gun->GetParticleEnergy() / MeV);
    VLOG_F(2, "DirectionX=%f [mm]", gun->GetParticleMomentumDirection().getX() / mm);
    VLOG_F(2, "DirectionY=%f [mm]", gun->GetParticleMomentumDirection().getY() / mm);
    VLOG_F(2, "DirectionZ=%f [mm]", gun->GetParticleMomentumDirection().getZ() / mm);
    VLOG_F(2, "PositionX=%f [mm]", gun->GetParticlePosition().getX() / mm);
    VLOG_F(2, "PositionY=%f [mm]", gun->GetParticlePosition().getY() / mm);
    VLOG_F(2, "PositionZ=%f [mm]", gun->GetParticlePosition().getZ() / mm);

    return gun;
  };
}
