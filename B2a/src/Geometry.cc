/// \file B2/B2a/src/Geometry.cc
/// \brief Implementation of the B2a::DetectorConstruction class

#include "Geometry.hh"
#include "DetectorMessenger.hh"
#include "TrackerSD.hh"

#include "G4AutoDelete.hh"
#include "G4Box.hh"
#include "G4Colour.hh"
#include "G4GeometryManager.hh"
#include "G4GeometryTolerance.hh"
#include "G4GlobalMagFieldMessenger.hh"
#include "G4LogicalVolume.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4SDManager.hh"
#include "G4SystemOfUnits.hh"
#include "G4Tubs.hh"
#include "G4UserLimits.hh"
#include "G4VisAttributes.hh"

#include <loguru.hpp>

using namespace B2;

namespace B2a
{
  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  // G4ThreadLocal G4GlobalMagFieldMessenger *Geometry::fMagFieldMessenger = nullptr;

  Geometry::Geometry()
  {
    LOG_F(INFO, "Geometry()");
    // fMessenger = new DetectorMessenger(this);

    // fNbOfChambers = 3;
    // fLogicChamber = new G4LogicalVolume *[fNbOfChambers];
  };

  // __________________________________________________
  Geometry::~Geometry()
  {
    LOG_F(INFO, "~Geometry()");
  };

  // __________________________________________________
  G4VPhysicalVolume *Geometry::Construct()
  {
    LOG_SCOPE_F(INFO, "Construct()");
    return SetupVolumes();
  };

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
  void Geometry::ConstructSDandField()
  {
    LOG_SCOPE_F(INFO, "ConstructSDandField()");

    // SDを設定
    // - TrackerSD: OSECHI
    // - ShieldSD: シールド
    // どちらも同じTrackerSDクラス
    // 論理ボリューム名を使って一括で設定

    // Shield
    // FIX: 0枚のときに論理ボリュームが存在しないため、
    // エラーになる。あとで考える。
    auto aShieldSD = new TrackerSD("/ShieldSD", "TrackerHitsCollection");
    SetSensitiveDetector(fShieldLVName, aShieldSD, true);

    // Tracker
    auto aTrackerSD = new TrackerSD("/TrackerSD", "TrackerHitsCollection");
    SetSensitiveDetector(fLayerLVName, aTrackerSD, true);

    auto sm = G4SDManager::GetSDMpointer();
    VLOG_F(1, "AddNewDetector(%s)", aShieldSD->GetName().c_str());
    sm->AddNewDetector(aShieldSD);
    VLOG_F(1, "AddNewDetector(%s)", aTrackerSD->GetName().c_str());
    sm->AddNewDetector(aTrackerSD);
  };

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  void Geometry::SetCheckOverlaps(G4bool checkOverlaps)
  {
    fCheckOverlaps = checkOverlaps;
  };

  // __________________________________________________
  G4LogicalVolume *Geometry::SetupLogicalBox(
      const G4String &logical_name,
      const G4String &material_name,
      const G4double full_length_x,
      const G4double full_length_y,
      const G4double full_length_z)
  {
    VLOG_SCOPE_F(4, "SetupLogicalBox");

    // Parameters
    G4double half_x = full_length_x * 0.5;
    G4double half_y = full_length_y * 0.5;
    G4double half_z = full_length_z * 0.5;

    // 形状
    auto solid = new G4Box{
        "solidBox",
        half_x,
        half_y,
        half_z,
    };

    // 材質
    auto nm = G4NistManager::Instance();
    auto material = nm->FindOrBuildMaterial(material_name);

    // 論理ボリューム
    G4LogicalVolume *logical = new G4LogicalVolume{
        solid,
        material,
        logical_name,
        nullptr,
        nullptr,
        nullptr,
    };

    // 確認用のログ出力
    VLOG_F(5, "SolidVolume: %s", solid->GetName().c_str());
    VLOG_F(5, "LogicalVolume: %s", logical->GetName().c_str());
    VLOG_F(5, "Size x: %f [cm]", 2 * half_x / cm);
    VLOG_F(5, "Size y: %f [cm]", 2 * half_y / cm);
    VLOG_F(5, "Size z: %f [cm]", 2 * half_z / cm);
    VLOG_F(5, "Material: %s", material->GetName().c_str());

    // 確認用の出力
    G4debug << material << G4endl;

    return logical;
  };

  // __________________________________________________
  G4LogicalVolume *Geometry::SetupWorldVolume()
  {
    VLOG_SCOPE_F(3, "SetupWorldVolume()");

    auto logical = SetupLogicalBox(
        fWorldLVName,
        fWorldMaterial,
        fWorldX,
        fWorldY,
        fWorldZ);

    // お化粧
    G4VisAttributes attr = G4Colour::Cyan();
    logical->SetVisAttributes(attr);

    return logical;
  };

  // __________________________________________________
  G4LogicalVolume *Geometry::SetupShieldVolume()
  {
    VLOG_SCOPE_F(3, "SetupShieldVolume()");

    auto logical = SetupLogicalBox(
        fShieldLVName,
        fShieldMaterial,
        fShieldX,
        fShieldY,
        fShieldZ);

    // 可視化
    G4VisAttributes attr = G4Colour::White();
    logical->SetVisAttributes(attr);

    //
    // 検出器ごとにSD設定することもできるが、
    // 今回は ConstrustSDandField で一括設定する
    //
    // auto detector = new TrackerSD("/ShieldSD", "TrackerHitsCollection");
    // logical->SetSensitiveDetector(detector);
    // auto sm = G4SDManager::GetSDMpointer();
    // sm->AddNewDetector(detector);

    return logical;
  };

  // __________________________________________________
  G4LogicalVolume *Geometry::SetupLayerVolume()
  {
    VLOG_SCOPE_F(3, "SetupLayerVolume()");

    auto logical = SetupLogicalBox(
        fLayerLVName,
        fLayerMaterial,
        fLayerX,
        fLayerY,
        fLayerZ);

    // 可視化
    G4VisAttributes attr = G4Colour::Yellow();
    logical->SetVisAttributes(attr);

    // auto detector = new TrackerSD("/ShieldSD", "TrackerHitsCollection");
    // logical->SetSensitiveDetector(detector);
    // auto sm = G4SDManager::GetSDMpointer();
    // sm->AddNewDetector(detector);

    return logical;
  };

  // __________________________________________________
  G4LogicalVolume *Geometry::SetupOsechi(G4int n_elements, const G4String &osechi_name)
  {
    VLOG_SCOPE_F(3, "SetupOsechi(%d): %s", n_elements, osechi_name.c_str());

    // 子ボリューム（＝シンチレーター1枚）
    G4LogicalVolume *element = SetupLayerVolume();

    // 親ボリューム（＝OSECHIの箱）
    G4LogicalVolume *container = SetupLogicalBox(
        "ContainerBox",
        "G4_AIR",
        fLayerX,
        fLayerY,
        fLayerZ * n_elements // 子ボリュームの数だけ厚みが増えた
    );

    new G4PVReplica{
        osechi_name, // OSECHIの名前
        element,     // 子ボリューム
        container,   // 親ボリューム
        kZAxis,      // レプリカ方向
        n_elements,  // レプリカ数
        fLayerZ      // レプリカ長（＝子ボリュームのサイズ）
    };

    return container;
  };

  // __________________________________________________
  G4LogicalVolume *Geometry::SetupShields(G4int n_elements, const G4String &shield_name)
  {

    // 0枚のときはスキップする
    if (n_elements == 0)
    {
      return nullptr;
    }

    VLOG_SCOPE_F(3, "SetupShields(%d)", n_elements);

    // 子ボリューム（＝シールド x 1枚）
    G4LogicalVolume *element = SetupShieldVolume();

    // 親ボリューム（＝シールド x n_elements 枚）
    G4LogicalVolume *container = SetupLogicalBox(
        "ContainerBox",
        "G4_AIR",
        fShieldX,
        fShieldY,
        fShieldZ * n_elements // 子ボリュームの数だけ厚みが増えた
    );

    new G4PVReplica{
        shield_name, // シールドの名前
        element,     // 子ボリューム
        container,   // 親ボリューム
        kZAxis,      // レプリカ方向
        n_elements,  // レプリカ数
        fShieldZ     // レプリカ長（＝子ボリュームのサイズ）
    };

    return container;
  };

  // __________________________________________________
  G4VPhysicalVolume *Geometry::SetupVolumes()
  {
    VLOG_SCOPE_F(2, "SetupVolumes()");

    // スコープ内のパラメーター
    G4RotationMatrix rotation{};
    G4ThreeVector direction{};
    G4Transform3D origin{};

    // Setup World
    auto worldLV = SetupWorldVolume();
    rotation = G4RotationMatrix{};
    direction = G4ThreeVector{};
    origin = G4Transform3D{rotation, direction};
    auto theWorld = new G4PVPlacement{
        origin,
        worldLV,       // its logical volume
        "world",       // its name
        nullptr,       // its mother  volume
        false,         // no boolean operations
        0,             // copy number
        fCheckOverlaps // checking overlaps
    };

    // Setup Shield
    auto shieldLV = SetupShields(1, "shields");
    if (shieldLV != nullptr)
    {
      rotation = G4RotationMatrix{};
      direction = G4ThreeVector{0., 0., -50. * cm};
      origin = G4Transform3D{rotation, direction};
      auto theShield = new G4PVPlacement{
          origin,
          shieldLV,
          "shield",
          worldLV,
          false,
          0,
          fCheckOverlaps};
    };

    // Setup Osechi
    auto osechiLV = SetupOsechi(3, "osechi1");

    rotation = G4RotationMatrix{};
    direction = G4ThreeVector{0., 0., 0 * cm};
    origin = G4Transform3D{rotation, direction};
    new G4PVPlacement{
        origin,
        osechiLV,
        "osechi",
        worldLV,
        false,
        0,
        fCheckOverlaps};

    // rotation = G4RotationMatrix{};
    // direction = G4ThreeVector{0., 0., 90 * cm};
    // origin = G4Transform3D{rotation, direction};
    // new G4PVPlacement{
    // origin,
    // osechiLV,
    //"osechi2",
    // worldLV,
    // false,
    // 0,
    // fCheckOverlaps};

    return theWorld;
  }
}