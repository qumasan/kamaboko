//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file B2/B2a/src/EventAction.cc
/// \brief Implementation of the B2::EventAction class

#include "EventAction.hh"

#include "G4AnalysisManager.hh"
#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4Trajectory.hh"
#include "G4TrajectoryContainer.hh"
#include "G4ios.hh"

#include <loguru.hpp>
namespace B2
{

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  void EventAction::BeginOfEventAction(const G4Event *aEvent)
  {
    LOG_F(INFO, "|- BeginOfEventAction");
    LOG_F(INFO, "Check Random seeds");
    LOG_F(INFO, "\tCLHEP::HepRandom::getTheSeed: %ld", CLHEP::HepRandom::getTheSeed());
    // G4debug << "\tG4Event::GetRandomNumberStatus=" << aEvent->GetRandomNumberStatus() << G4endl;
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  void EventAction::EndOfEventAction(const G4Event *aEvent)
  {
    LOG_F(INFO, "|- EndOfEventAction");

    // get number of stored trajectories
    G4TrajectoryContainer *trajectoryContainer = aEvent->GetTrajectoryContainer();
    std::size_t n_trajectories = 0;
    if (trajectoryContainer)
      n_trajectories = trajectoryContainer->entries();

    auto am = G4AnalysisManager::Instance();
    G4int h1 = am->GetH1Id("h1");

    G4int numberOfVertex = aEvent->GetNumberOfPrimaryVertex();
    LOG_F(INFO, "numberOfVertex: %d", numberOfVertex);

    am->FillH1(h1, 1);
    am->FillH1(h1, 1);
    am->FillH1(h1, 1);
    am->FillH1(h1, 3);
    am->FillH1(h1, 10);

    // periodic printing
    G4int eventID = aEvent->GetEventID();
    if (eventID < 100 || eventID % 100 == 0)
    {
      G4cout << ">>> EventID: " << eventID << G4endl;
      if (trajectoryContainer)
      {
        G4cout << "    " << n_trajectories
               << " trajectories stored in this event." << G4endl;
      }
      G4VHitsCollection *hc = aEvent->GetHCofThisEvent()->GetHC(0);
      G4cout << "    "
             << hc->GetSize() << " hits stored in this event" << G4endl;
    }
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

}
