/// \file B2/B2a/src/DetectorConstruction.cc
/// \brief Implementation of the B2a::DetectorConstruction class

#include "DetectorConstruction.hh"
#include "DetectorMessenger.hh"
#include "TrackerSD.hh"

#include "G4AutoDelete.hh"
#include "G4Box.hh"
#include "G4Colour.hh"
#include "G4GeometryManager.hh"
#include "G4GeometryTolerance.hh"
#include "G4GlobalMagFieldMessenger.hh"
#include "G4LogicalVolume.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4SDManager.hh"
#include "G4SystemOfUnits.hh"
#include "G4Tubs.hh"
#include "G4UserLimits.hh"
#include "G4VisAttributes.hh"

#include <loguru.hpp>

using namespace B2;

namespace B2a
{
  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  G4ThreadLocal G4GlobalMagFieldMessenger *DetectorConstruction::fMagFieldMessenger = nullptr;

  DetectorConstruction::DetectorConstruction()
  {
    LOG_F(INFO, "=====> DetectorConstruction");
    fMessenger = new DetectorMessenger(this);

    fNbOfChambers = 3;
    fLogicChamber = new G4LogicalVolume *[fNbOfChambers];
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  DetectorConstruction::~DetectorConstruction()
  {
    LOG_F(INFO, "<===== ~DetectorConstruction");
    delete[] fLogicChamber;
    delete fStepLimit;
    delete fMessenger;
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
  /**
   * @brief 測定器を構築
   *
   * 測定器を構築するための必須メソッドです。
   */
  G4VPhysicalVolume *DetectorConstruction::Construct()
  {
    LOG_F(INFO, "|- Construct()");
    // Define materials
    DefineMaterials();

    // Define volumes
    // return DefineVolumes();
    return SetupVolumes();
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  void DetectorConstruction::DefineMaterials()
  {
    LOG_F(INFO, "|-- DefineMaterials()");
    // Material definition

    auto nm = G4NistManager::Instance();

    // Air defined using NIST Manager
    nm->FindOrBuildMaterial("G4_AIR");

    // Lead defined using NIST Manager
    fTargetMaterial = nm->FindOrBuildMaterial("G4_Pb");

    // Xenon gas defined using NIST Manager
    nm->FindOrBuildMaterial("G4_Xe");

    // Xenon gas defined using NIST Manager
    nm->FindOrBuildMaterial("G4_WATER");

    // Plastic scintillator using NIST Manager
    fChamberMaterial = nm->FindOrBuildMaterial("G4_PLASTIC_SC_VINYLTOLUENE");

    // Print materials
    G4debug << *(G4Material::GetMaterialTable()) << G4endl;
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  G4VPhysicalVolume *DetectorConstruction::DefineVolumes()
  {
    LOG_F(INFO, ">> DefineVolumes()");
    G4Material *air = G4Material::GetMaterial("G4_AIR");

    // Sizes of the principal geometrical components (solids)

    G4double chamberSpacing = 1.0 * cm; // from chamber center to center!

    G4double chamberWidth = 1.0 * cm; // width of the chambers
    G4double targetLength = 5.0 * cm; // full length of Target

    G4double trackerLength = (fNbOfChambers + 1) * chamberSpacing;

    G4double worldLength = 1.2 * (2 * targetLength + trackerLength);

    G4double targetRadius = 0.5 * targetLength; // Radius of Target
    targetLength = 0.5 * targetLength;          // Half length of the Target
    G4double trackerSize = 0.5 * trackerLength; // Half length of the Tracker

    // Definitions of Solids, Logical Volumes, Physical Volumes

    // World

    G4GeometryManager::GetInstance()->SetWorldMaximumExtent(worldLength);

    G4cout << "Computed tolerance = "
           << G4GeometryTolerance::GetInstance()->GetSurfaceTolerance() / mm
           << " mm" << G4endl;
    // auto worldLV = CreateWorldVolume("World");
    auto worldLV = SetupWorldVolume();

    //  Must place the World Physical volume unrotated at (0,0,0).
    //
    // World
    G4RotationMatrix worldRotation{};
    G4ThreeVector worldDirection{};
    G4Transform3D worldOrigin{worldRotation, worldDirection};
    auto worldPV = new G4PVPlacement{
        worldOrigin,
        worldLV,       // its logical volume
        "World",       // its name
        nullptr,       // its mother  volume
        false,         // no boolean operations
        0,             // copy number
        fCheckOverlaps // checking overlaps
    };

    // Target
    // TODO: Target -> Shieldに変更する
    // fLogicTarget = CreateShieldVolume("ShieldLogical");
    fLogicTarget = SetupShieldVolume();
    G4ThreeVector positionTarget = G4ThreeVector{0, 0, -10 * cm};
    new G4PVPlacement{
        nullptr,        // no rotation
        positionTarget, // at (x,y,z)
        fLogicTarget,   // its logical volume
        "Target",       // its name
        worldLV,        // its mother volume
        false,          // no boolean operations
        0,              // copy number
        fCheckOverlaps  // checking overlaps
    };

    // Tracker
    G4ThreeVector positionTracker = G4ThreeVector{0, 0, 0};
    auto trackerS = new G4Box{"tracker", 6.0 * cm, 3.0 * cm, trackerSize};
    auto trackerLV = new G4LogicalVolume{trackerS, air, "Tracker", nullptr, nullptr, nullptr};
    new G4PVPlacement{
        nullptr,         // no rotation
        positionTracker, // at (x,y,z)
        trackerLV,       // its logical volume
        "Tracker",       // its name
        worldLV,         // its mother  volume
        false,           // no boolean operations
        0,               // copy number
        fCheckOverlaps   // checking overlaps
    };

    // Visualization attributes

    G4VisAttributes boxVisAtt(G4Colour::White());
    G4VisAttributes chamberVisAtt(G4Colour::Yellow());
    trackerLV->SetVisAttributes(boxVisAtt);

    // Tracker segments

    G4cout << "There are " << fNbOfChambers << " chambers in the tracker region. "
           << G4endl
           << "The chambers are " << chamberWidth / cm << " cm of "
           << fChamberMaterial->GetName() << G4endl
           << "The distance between chamber is " << chamberSpacing / cm << " cm"
           << G4endl;

    G4double firstPosition = -trackerSize + chamberSpacing;
    G4double firstLength = trackerLength / 10;
    G4double lastLength = trackerLength;

    G4double halfWidth = 0.5 * chamberWidth;
    G4double rmaxFirst = 0.5 * firstLength;

    G4double rmaxIncr = 0.0;
    if (fNbOfChambers > 0)
    {
      rmaxIncr = 0.5 * (lastLength - firstLength) / (fNbOfChambers - 1);
      if (chamberSpacing < chamberWidth)
      {
        G4Exception("DetectorConstruction::DefineVolumes()",
                    "InvalidSetup", FatalException,
                    "Width>Spacing");
      }
    }

    for (G4int copyNo = 0; copyNo < fNbOfChambers; copyNo++)
    {

      G4double Zposition = firstPosition + copyNo * chamberSpacing;
      G4double rmax = rmaxFirst + copyNo * rmaxIncr;

      // auto chamberS = new G4Tubs("Chamber_solid", 0, rmax, halfWidth, 0. * deg, 360. * deg);
      auto chamberS = new G4Box("Chamber_solid", 5 * cm, 2.5 * cm, 0.5 * cm);

      fLogicChamber[copyNo] =
          new G4LogicalVolume(chamberS, fChamberMaterial, "Chamber_LV", nullptr, nullptr, nullptr);

      fLogicChamber[copyNo]->SetVisAttributes(chamberVisAtt);

      new G4PVPlacement(nullptr,                        // no rotation
                        G4ThreeVector(0, 0, Zposition), // at (x,y,z)
                        fLogicChamber[copyNo],          // its logical volume
                        "Chamber_PV",                   // its name
                        trackerLV,                      // its mother  volume
                        false,                          // no boolean operations
                        copyNo,                         // copy number
                        fCheckOverlaps);                // checking overlaps
    }

    // Example of User Limits
    //
    // Below is an example of how to set tracking constraints in a given
    // logical volume
    //
    // Sets a max step length in the tracker region, with G4StepLimiter

    G4double maxStep = 0.5 * chamberWidth;
    fStepLimit = new G4UserLimits(maxStep);
    trackerLV->SetUserLimits(fStepLimit);

    /// Set additional contraints on the track, with G4UserSpecialCuts
    ///
    /// G4double maxLength = 2*trackerLength, maxTime = 0.1*ns, minEkin = 10*MeV;
    /// trackerLV->SetUserLimits(new G4UserLimits(maxStep,
    ///                                           maxLength,
    ///                                           maxTime,
    ///                                           minEkin));

    // Always return the physical world

    return worldPV;
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  void DetectorConstruction::ConstructSDandField()
  {
    LOG_F(INFO, "|- ConstructSDandField()");

    // SDを設定
    // - TrackerSD: OSECHI
    // - ShieldSD: シールド
    // どちらも同じTrackerSDクラス
    // 論理ボリューム名を使って一括で設定

    // Tracker
    auto aTrackerSD = new TrackerSD("/TrackerSD", "TrackerHitsCollection");
    SetSensitiveDetector(fLayerLVName, aTrackerSD, true);
    // Shield
    auto aShieldSD = new TrackerSD("/ShieldSD", "ShieldHitsCollection");
    SetSensitiveDetector(fShieldLVName, aShieldSD, true);

    auto sm = G4SDManager::GetSDMpointer();
    sm->AddNewDetector(aTrackerSD);
    sm->AddNewDetector(aShieldSD);

    // Create global magnetic field messenger.
    // Uniform magnetic field is then created automatically if
    // the field value is not zero.
    G4ThreeVector fieldValue = G4ThreeVector();
    fMagFieldMessenger = new G4GlobalMagFieldMessenger(fieldValue);
    fMagFieldMessenger->SetVerboseLevel(1);

    // Register the field messenger for deleting
    G4AutoDelete::Register(fMagFieldMessenger);
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  void DetectorConstruction::SetTargetMaterial(G4String materialName)
  {
    LOG_F(INFO, ">> SetTargetMaterial(%s)", materialName.c_str());
    G4NistManager *nistManager = G4NistManager::Instance();

    G4Material *pttoMaterial =
        nistManager->FindOrBuildMaterial(materialName);

    if (fTargetMaterial != pttoMaterial)
    {
      if (pttoMaterial)
      {
        fTargetMaterial = pttoMaterial;
        if (fLogicTarget)
          fLogicTarget->SetMaterial(fTargetMaterial);
        G4cout
            << G4endl
            << "----> The target is made of " << materialName << G4endl;
      }
      else
      {
        G4cout
            << G4endl
            << "-->  WARNING from SetTargetMaterial : "
            << materialName << " not found" << G4endl;
      }
    }
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  void DetectorConstruction::SetChamberMaterial(G4String materialName)
  {
    LOG_F(INFO, ">> SetChamberMaterial(%s)", materialName.c_str());
    G4NistManager *nistManager = G4NistManager::Instance();

    G4Material *pttoMaterial =
        nistManager->FindOrBuildMaterial(materialName);

    if (fChamberMaterial != pttoMaterial)
    {
      if (pttoMaterial)
      {
        fChamberMaterial = pttoMaterial;
        for (G4int copyNo = 0; copyNo < fNbOfChambers; copyNo++)
        {
          if (fLogicChamber[copyNo])
            fLogicChamber[copyNo]->SetMaterial(fChamberMaterial);
        }
        G4cout
            << G4endl
            << "----> The chambers are made of " << materialName << G4endl;
      }
      else
      {
        G4cout
            << G4endl
            << "-->  WARNING from SetChamberMaterial : "
            << materialName << " not found" << G4endl;
      }
    }
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  void DetectorConstruction::SetMaxStep(G4double maxStep)
  {
    LOG_F(INFO, ">> SetMaxStep(%f)", maxStep);
    if ((fStepLimit) && (maxStep > 0.))
      fStepLimit->SetMaxAllowedStep(maxStep);
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  void DetectorConstruction::SetCheckOverlaps(G4bool checkOverlaps)
  {
    fCheckOverlaps = checkOverlaps;
  }

  // __________________________________________________
  G4LogicalVolume *DetectorConstruction::SetupLogicalBox(
      const G4String &logical_name,
      const G4String &material_name,
      const G4double full_length_x,
      const G4double full_length_y,
      const G4double full_length_z)
  {
    LOG_F(INFO, "|---- SetupLogicalBox");

    // Parameters
    G4double half_x = full_length_x * 0.5;
    G4double half_y = full_length_y * 0.5;
    G4double half_z = full_length_z * 0.5;

    // 形状
    auto solid = new G4Box{
        "solidBox",
        half_x,
        half_y,
        half_z,
    };

    // 材質
    auto nm = G4NistManager::Instance();
    auto material = nm->FindOrBuildMaterial(material_name);

    // 論理ボリューム
    G4LogicalVolume *logical = new G4LogicalVolume{
        solid,
        material,
        logical_name,
        nullptr,
        nullptr,
        nullptr,
    };

    // 確認用のログ出力
    LOG_F(INFO, "SolidVolume: %s", solid->GetName().c_str());
    LOG_F(INFO, "LogicalVolume: %s", logical->GetName().c_str());
    LOG_F(INFO, "Size x: %f [cm]", 2 * half_x / cm);
    LOG_F(INFO, "Size y: %f [cm]", 2 * half_y / cm);
    LOG_F(INFO, "Size z: %f [cm]", 2 * half_z / cm);
    LOG_F(INFO, "Material: %s", material->GetName().c_str());

    // 確認用の出力
    G4debug << material << G4endl;

    return logical;
  };

  // __________________________________________________
  G4LogicalVolume *DetectorConstruction::SetupWorldVolume()
  {
    LOG_F(INFO, "|--- SetupWorldVolume()");

    auto logical = SetupLogicalBox(
        fWorldLVName,
        fWorldMaterial,
        fWorldX,
        fWorldY,
        fWorldZ);

    // お化粧
    G4VisAttributes attr = G4Colour::Cyan();
    logical->SetVisAttributes(attr);

    return logical;
  };

  // __________________________________________________
  G4LogicalVolume *DetectorConstruction::SetupShieldVolume()
  {
    LOG_F(INFO, "|--- SetupShieldVolume()");

    auto logical = SetupLogicalBox(
        fShieldLVName,
        fShieldMaterial,
        fShieldX,
        fShieldY,
        fShieldZ);

    // 可視化
    G4VisAttributes attr = G4Colour::Red();
    logical->SetVisAttributes(attr);

    auto detector = new TrackerSD("/ShieldSD", "TrackerHitsCollection");
    logical->SetSensitiveDetector(detector);
    auto sm = G4SDManager::GetSDMpointer();
    sm->AddNewDetector(detector);

    return logical;
  };

  // __________________________________________________
  G4LogicalVolume *DetectorConstruction::SetupLayerVolume()
  {
    LOG_F(INFO, "|--- SetupLayerVolume()");

    auto logical = SetupLogicalBox(
        fLayerLVName,
        fLayerMaterial,
        fLayerX,
        fLayerY,
        fLayerZ);

    // 可視化
    G4VisAttributes attr = G4Colour::Yellow();
    logical->SetVisAttributes(attr);

    // auto detector = new TrackerSD("/ShieldSD", "TrackerHitsCollection");
    // logical->SetSensitiveDetector(detector);
    // auto sm = G4SDManager::GetSDMpointer();
    // sm->AddNewDetector(detector);

    return logical;
  };

  // __________________________________________________
  G4LogicalVolume *DetectorConstruction::SetupOsechi(G4int n_elements)
  {
    LOG_F(INFO, "|--- SetupOsechi()");

    // 子ボリューム（＝シンチレーター1枚）
    G4LogicalVolume *element = SetupLayerVolume();

    // 親ボリューム（＝OSECHIの箱）
    G4LogicalVolume *container = SetupLogicalBox(
        "ContainerBox",
        "G4_AIR",
        fLayerX,
        fLayerY,
        fLayerZ * n_elements // 子ボリュームの数だけ厚みが増えた
    );

    new G4PVReplica{
        "Osechi",
        element,    // 子ボリューム
        container,  // 親ボリューム
        kZAxis,     // レプリカ方向
        n_elements, // レプリカ数
        fLayerZ     // レプリカ長（＝子ボリュームのサイズ）
    };

    return container;
  };

  // __________________________________________________
  G4VPhysicalVolume *DetectorConstruction::SetupVolumes()
  {
    LOG_F(INFO, "|-- SetupVolumes()");

    // スコープ内のパラメーター
    G4RotationMatrix rotation{};
    G4ThreeVector direction{};
    G4Transform3D origin{};

    // Setup World
    auto worldLV = SetupWorldVolume();
    rotation = G4RotationMatrix{};
    direction = G4ThreeVector{};
    origin = G4Transform3D{rotation, direction};
    auto theWorld = new G4PVPlacement{
        origin,
        worldLV,       // its logical volume
        "world",       // its name
        nullptr,       // its mother  volume
        false,         // no boolean operations
        0,             // copy number
        fCheckOverlaps // checking overlaps
    };

    // Setup Shield
    auto shieldLV = SetupShieldVolume();
    rotation = G4RotationMatrix{};
    direction = G4ThreeVector{0., 0., -5. * cm};
    origin = G4Transform3D{rotation, direction};
    auto theShield = new G4PVPlacement{
        origin,
        shieldLV,
        "shield",
        worldLV,
        false,
        0,
        fCheckOverlaps};

    // Setup Osechi
    auto osechiLV = SetupOsechi(3);
    rotation = G4RotationMatrix{};
    direction = G4ThreeVector{0., 0., 0 * cm};
    origin = G4Transform3D{rotation, direction};
    auto theOsechi = new G4PVPlacement{
        origin,
        osechiLV,
        "osechi",
        worldLV,
        false,
        0,
        fCheckOverlaps};

    return theWorld;
  }
}