//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file B2/B2a/src/RunAction.cc
/// \brief Implementation of the B2::RunAction class

#include "RunAction.hh"

#include <chrono>  // std::chrono::system_clock
#include <ctime>   // std::time_t, std::tm, localtime_r
#include <iomanip> // std::put_time
#include <sstream> // std::stringstream

#include "G4AnalysisManager.hh"
#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4SDManager.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"

#include <loguru.hpp>
namespace
{
  // 無名の名前空間に入れて、スコープを小さくする
  /**
   * @fn
   * 時刻付きのファイル名を生成するローカル関数
   * @params (aPrefix) ファイル名のプリフィックス
   * @params (aSuffix) ファイル名のサフィックス
   * @params (aSaveTo) ファイルを保存するディレクトリ名
   * @return 時刻付きのファイル名 {aSaveTo}/{aPrefix}_{yyyy-mm-ddTHHhMMmSSs}.{aSuffix}
   */
  G4String getFileName(const G4String &aPrefix, const G4String &aSuffix, const G4String &aSaveTo = ".")
  {

    // 現在のシステム時刻を取得
    auto now = std::chrono::system_clock::now();
    // time_t形式に変換
    std::time_t now_t = std::chrono::system_clock::to_time_t(now);
    // tm構造体に変換
    std::tm now_tm;
    localtime_r(&now_t, &now_tm);

    // 時刻をフォーマット
    std::stringstream ymd;
    ymd << std::put_time(&now_tm, "%Y-%m-%dT%Hh%Mm%Ss");

    // ファイル名を生成
    // fileName = "{aSaveTo}/{aPrefix}_{yyyy-mm-ddTHHhMMmSSs}.{aSuffix}"
    std::stringstream fileName;
    fileName << aSaveTo << "/" << aPrefix << "_" << ymd.str() << "." << aSuffix;
    return fileName.str();
  }
}

namespace B2
{
  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
  RunAction::RunAction()
  {
    LOG_F(INFO, "=====> RunAction");

    // set printing event number per each 100 events
    G4RunManager::GetRunManager()->SetPrintProgress(1000);

    // 解析マネージャーを作成する
    auto am = G4AnalysisManager::Instance();

    // 1Dヒストグラムを作成する
    am->CreateH1("h1", "test", 100, 0, 100);
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  void RunAction::BeginOfRunAction(const G4Run *aRun)
  {
    LOG_F(INFO, "|- BeginOfRunAction");

    // inform the runManager to save random number seed
    G4RunManager::GetRunManager()->SetRandomNumberStore(false);

    auto am = G4AnalysisManager::Instance();
    auto fileName = getFileName("kamaboko", "csv", "mc_data");
    am->SetDefaultFileType("csv");
    am->SetFileName(fileName);
    am->OpenFile();
    LOG_F(INFO, "File opened: %s", am->GetFileName().c_str());

    // G4debug << "[BeginOfRunAction] Check Random seeds: " << G4endl;
    // G4debug << "\tG4Run::GetRandomNumberStatus=" << aRun->GetRandomNumberStatus() << G4endl;
    // G4debug << "\tG4Random::getTheSeed=" << G4Random::getTheSeed() << G4endl;
    // G4debug << "\tG4Random::getTheSeeds=" << G4Random::getTheSeeds() << G4endl;

    CLHEP::HepRandom::saveEngineStatus();
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  void RunAction::EndOfRunAction(const G4Run *aRun)
  {
    LOG_F(INFO, "|- EndOfRunAction");

    auto am = G4AnalysisManager::Instance();
    am->Write();
    am->CloseFile();

    LOG_F(INFO, "File closed: %s", am->GetFileName().c_str());

    // G4debug << "[EndOfRunAction] Check Random seeds: " << G4endl;
    // G4debug << "\tG4Run::GetRandomNumberStatus=" << aRun->GetRandomNumberStatus() << G4endl;
    // G4debug << "\tG4CLHEP::HepRandom::getTheSeed=" << CLHEP::HepRandom::getTheSeed() << G4endl;
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
}
