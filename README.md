# kamaboko（かまぼこ）

Geant4 detector simulation kit for OSECHI

![](./mu100_1GeV.png)


## Description

小型宇宙線検出器OSECHIの検出器シミュレーションを開発しています。

## Setup Geant4

```console
$ zsh
$ source ~/geant4/11.2.1/bin/geant4.sh
```

1. Zshを起動する
2. Geant4提供のスクリプトで環境変数を設定する

## Getting started

```console
$ cd ~/repos/
$ git clone https://gitlab.com/qumasan/kamaboko.git
$ cd kamaboko
```

1. 作業用ディレクトリに移動する
2. リポジトリをクローンする

## Build & Run Geant4 Application

```console
// ビルド用ディレクトリを作成する
(kamaboko/) $ mkdir build
(kamaboko/) $ cd build

// 設定ファイルを生成する
(kamaboko/build/) $ cmake ../B2a/

// アプリケーションをビルドする
(kamaboko/build/) $ make -j8

// アプリケーションを実行する
(kamaboko/build/) $ ./exampleB2a
```

1. プロジェクト内（``kamaboko``）にビルド用ディレクトリ（``build``）を作成する
2. ``cmake``で設定ファイルを生成する
3. ``make``でアプリケーションをビルドする
4. ``.exampleB2a``でアプリケーションを実行する

## VS Codeで開発する

``.vscode/settings.json``を作成し、CMake環境を整理した

パスの設定は以下のとおり

- ``cmake`` : ``/opt/homebrew/bin/cmake``
- ``${workspaceFolder}``: プロジェクトルート

コマンドパレットを開いて、以下のコマンドを検索＆実行する

- ``CMake: Configure``: ``cmake -Dオプション -S B2a -B build -G "Unix Makefiles``を実行
- ``CMake: Build``: ``cmake --build build --config Debug --target all -j 10``を実行
- ``CMake: Install``: ``cmake --build build --config Debug --target install -j 10``を実行
- ``CMake: Clean``
- ``CMake: Clean Rebuild``

## データ形式

- シミュレーション結果はLTSV形式で出力される
  - 正確にはLTSV亜種（独自形式）
  - Excelなどで開いて確認しやすいように、Tab-Separatedではなく、Comma-Separatedにした
- LTSV形式（亜種）にすることで、解析ツールを自由に選択できるようにした
  - 各自でLTSV形式をパースして、解析しやすい形に変換する
  - TODO: LTSV形式からCSV形式に変換するツール（→ Pandasなどで解析）
  - TODO: LTSV形式からROOT形式に変換するツール（→ ROOTで解析）

## 解析ツール

```console
// プロジェクトルートに移動
$ cd kamaboko

// Python環境を構築
$ rye sync

// Notebookを使って解析する
$ cd notebooks/
```

- 解析ツールはPython（Pandas / Jupyter Notebook）で用意した
- 解析に必要なPython環境は``rye``で構築＆管理している

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
